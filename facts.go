package facts

import (
	"math/rand"
)

type Facts interface {
	RandomFact() string
}

//SingleFacts a struct that holds the fact
type SingleFacts struct {
	Facts []string
}

//RandomFact returns a random fact from the facts pool
func (f SingleFacts) RandomFact() string {
	return f.Facts[rand.Intn(len(f.Facts))]
}

//WeightedFacts a struct that holds the fact
type WeightedFacts struct {
	Facts   []*SingleFacts
	Weights []int
}

//NewWeightedFacts creates a new set of weighted facts
func NewWeightedFacts() *WeightedFacts {
	return &WeightedFacts{}
}

//AddFacts adds the facts and their weight
func (wf *WeightedFacts) AddFacts(weight int, facts *SingleFacts) {
	wf.Facts = append(wf.Facts, facts)
	var factsPosition = len(wf.Facts) - 1
	for i := 0; i < weight; i++ {
		wf.Weights = append(wf.Weights, factsPosition)
	}
}

//RandomFact returns a random fact from the facts pool
func (wf WeightedFacts) RandomFact() string {
	typeOfFact := wf.Weights[rand.Intn(len(wf.Weights))]
	return wf.Facts[typeOfFact].RandomFact()
}
