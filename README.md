# go-facts

Just a simple holder for facts, and getting a random fact.


    import (
        "fmt",
        "bitbucket.org/sskelt01/go-facts"
    )

    var factholder facts.Facts
    // set up the facts
    if inNorrisMode {
        // some single source of facts
        factholder = facts.NewChuckNorrisFacts()
    } else {
        // add some weighted facts, this example there will be a 70% chance of
        // a kitten fact, and 30% change of 30% chance of a Chuck Norris fact.
        wfacts := facts.NewWeightedFacts()
        wfacts.AddFacts(7, facts.NewCatFacts())
        wfacts.AddFacts(3, facts.NewChuckNorrisFacts())
        factholder = wfacts
    }

    fmt.Printf("%s\n",factholder.RandomFact())